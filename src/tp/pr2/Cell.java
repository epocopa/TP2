/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr2;

import tp.pr2.multigames.GameRules;

public class Cell {
	private int value;

	public Cell(int value) {
		this.value = value;
	}

	// GET & SET
	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	
	public boolean isEmpty(){
		return value == 0;
	}
	
	public int doMerge(Cell neighbour, GameRules rules){
		return rules.merge(this, neighbour);
	}
	
	public boolean canMerge(Cell neighbour, GameRules rules) {
		return rules.canMerge(this, neighbour);
	}
	
	@Override
	public String toString() {
		return value != 0 ? String.valueOf(value) : " ";
	}
}
