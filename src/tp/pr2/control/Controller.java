/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr2.control;

import tp.pr2.GameType;
import tp.pr2.control.commands.Command;
import tp.pr2.multigames.*;
import tp.pr2.util.CommandParser;
import java.util.Random;
import java.util.Scanner;

public class Controller {
	private Game game;
	private Scanner in;
	private boolean printGameState = true;

	public Controller(Game game, Scanner in) {
		this.game = game;
		this.in = in;
	}

	public void run() {
		String[] commandWords;
		Command command;
		boolean cont = true;
		do {
			if (printGameState){
				System.out.println(game.toString());
			} else {
				printGameState = true;
			}
			System.out.print("~$ ");
			commandWords = in.nextLine().toLowerCase().trim().split(" ");
			command = CommandParser.parseCommand(commandWords, this);
			if (command != null) {
				command.execute(game, this);
			}
			else {
				System.out.println("Unknown command");
				setNotPrintGameState();
			}
		} while (!game.isEnded());
		System.out.println(game.toString());
		if (game.isWin()){
			System.out.println("Congratulations, you won :)");
		} else {
			System.out.println("Nice try, you lost :(");
		}
	}

	public void setNotPrintGameState() {
		printGameState = false;
	}

	public void newGame(GameType gameType) {
		int newSize = 4, newCells = 2;
		String aux;
		long newSeed;
		GameRules newRules;

		System.out.print("If the parameters are not provided or can't be used, the default parameters will be used.\n");
		System.out.print("Provide the new size: ");
		aux = in.nextLine();
		if (!aux.isEmpty()) {
			newSize = Integer.valueOf(aux);
		}


		System.out.print("Provide the number of initial cells: ");
		aux = in.nextLine();
		if (!aux.isEmpty()) {
			newCells = Integer.valueOf(aux);
		}

		System.out.print("Do you want to provide a seed? (Y/N) ");
		String seed = in.nextLine();
		if (seed.toLowerCase().equals("y")){
			System.out.print("Provide the new game seed: ");
			newSeed = in.nextInt();
		} else {
			newSeed = new Random().nextInt(1000);
		}

		switch(gameType){
			case ORIG:
				newRules = new Rules2048();
				break;
			case FIB:
				newRules = new RulesFib();
				break;
			case INV:
				newRules = new RulesInverse();
				break;
			default:
				newRules = new Rules2048();
				break;
		}

		game = new Game(newSize, newCells, newSeed, newRules);
	}
}