/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr2.control.commands;

import tp.pr2.GameType;
import tp.pr2.control.Controller;
import tp.pr2.multigames.Game;

public class PlayCommand extends Command {
	GameType type;
	private static String commandText = "play";
	private static String helpText = "Starts the desired game variant";

	public PlayCommand(GameType type) {
		super(commandText, helpText);
		this.type = type;
	}

	public PlayCommand() {
		super(commandText, helpText);
	}

	@Override
	public void execute(Game game, Controller controller) {
		controller.newGame(type);
	}

	@Override
	public Command parse(String[] commandWords, Controller controller) {
		Command command = null;
		boolean isPlayCommand = commandWords[0].equals(commandText);

		if (commandWords.length == 2 && isPlayCommand){
			try {
				GameType type = GameType.valueOf(commandWords[1].toUpperCase());
				command = new PlayCommand (type);
			} catch (IllegalArgumentException e) {
				System.err.println("No game type");
			}
		}
		return command;
	}
}
