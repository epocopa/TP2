/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr2.control.commands;

import tp.pr2.control.Controller;
import tp.pr2.multigames.Game;

import static tp.pr2.util.CommandParser.commandHelp;

public class HelpCommand extends NoParamsCommand {
	private static String commandText = "help";
	private static String helpText = "Restarts the game";

	public HelpCommand() {
		super(commandText, helpText);
	}

	@Override
	public void execute(Game game, Controller controller) {
		System.out.println(commandHelp());
		controller.setNotPrintGameState();
	}
	
	

}
