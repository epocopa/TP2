/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr2.control.commands;

import tp.pr2.Direction;
import tp.pr2.control.Controller;
import tp.pr2.multigames.Game;

public class MoveCommmand extends Command {
	Direction direction;
	private static String commandText = "move";
	private static String helpText = "Executes a move in the desired direction";



	public MoveCommmand(Direction direction) {
		super(commandText, helpText);
		this.direction = direction;
	}

	public MoveCommmand() {
		super(commandText, helpText);
	}

	@Override
	public void execute(Game game, Controller controller) {
		game.move(direction);
	}

	@Override
	public Command parse(String[] commandWords, Controller controller) {
		Command command = null;
		boolean isMoveCommand = commandWords[0].equals(commandText);

		if (commandWords.length == 2 && isMoveCommand){
			try {
				Direction dir = Direction.valueOf(commandWords[1].toUpperCase());
				command = new MoveCommmand (dir);
			} catch (IllegalArgumentException e) {
				System.err.println("No such direction");
			}
		}
		return command;
	}

}
