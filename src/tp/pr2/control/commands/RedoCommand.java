/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr2.control.commands;

import tp.pr2.control.Controller;
import tp.pr2.multigames.Game;

public class RedoCommand extends NoParamsCommand{
	private static String commandText = "redo";
	private static String helpText = "Redo";

	public RedoCommand() {
		super(commandText, helpText);
	}

	@Override
	public void execute(Game game, Controller controller) {
		if(!game.redo()){
			controller.setNotPrintGameState();
		}
	}
}
