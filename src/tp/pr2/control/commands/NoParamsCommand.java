/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr2.control.commands;

import tp.pr2.control.Controller;
import tp.pr2.multigames.Game;

public class NoParamsCommand extends Command {
	private static String commandText = "noparams";
	private static String helpText = "Commands without parameters";

	public NoParamsCommand(String commandInfo, String helpInfo) {
		super(commandInfo, helpInfo);
	}

	public NoParamsCommand() {
		super();
	}

	@Override
	public void execute(Game game, Controller controller) {

	}


	@Override
	public Command parse(String[] commandWords, Controller controller) {
		Command command = null;
		if (commandWords.length == 1){
			switch (commandWords[0]){
				case "exit":
					command = new ExitCommand();
					break;
				case "help":
					command = new HelpCommand();
					break;
				case "reset":
					command = new ResetCommand();
					break;
				case "undo":
					command = new UndoCommand();
					break;
				case "redo":
					command = new RedoCommand();
					break;
			}
		}
		return command;
	}
}
