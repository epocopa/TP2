/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr2;

import tp.pr2.multigames.GameRules;
import tp.pr2.util.MyStringUtils;

import java.util.Random;

public class Board {
	private Cell[][] board;
	private int boardSize;
	
	public Board(int boardSize) {
		this.boardSize = boardSize;
		board = new Cell[boardSize][boardSize];
		for(int i = 0; i < boardSize; i++){
			for(int j = 0; j < boardSize; j++){
				board[i][j] = new Cell(0);
			}
		}
	}
	
	public void setCell(Position pos, int value){
		board[pos.getRow()][pos.getColumn()].setValue(value);
	}
	
	public MoveResults executeMove(Direction dir, GameRules rules){
		MoveResults results;
		
		switch(dir){
			case DOWN:
				trans();
				mirror();
				results = move_left(rules);
				mirror();
				trans();
				break;
			case LEFT:
				results = move_left(rules);
				break;
			case RIGHT:
				mirror();
				results = move_left(rules);
				mirror();
				break;
			case UP:
				trans();
				results = move_left(rules);
				trans();
				break;
			default:
				results = move_left(rules);
				break;
		}
		
		
		return results;
	}
	
	private MoveResults move_left(GameRules rules) {
		boolean moved = false;
		int points = 0;
		
		// Push && Merge
		for(int i = 0; i < boardSize; i++) {
			int elm = 0;
			boolean lastMerged = true;
			if (!board[i][0].isEmpty()) {
				elm = 1;
				lastMerged = false;
			}
			for (int j = 1; j < boardSize; j++) {
				if (!board[i][j].isEmpty()) {
					if (!lastMerged && board[i][elm-1].canMerge(board[i][j], rules)) {
						points += board[i][elm-1].doMerge(board[i][j], rules);
						lastMerged = true;
						moved = true;
					} else {
						if (elm != j) {
							board[i][elm].setValue(board[i][j].getValue());
							board[i][j].setValue(0);
							lastMerged = false;
							moved = true;
						}
						elm++;
					}
				}
			}
		}
		
		return new MoveResults(moved, points);
	}
	
	private void mirror() {
		Cell aux;
		for (int i = 0; i < boardSize; i++) {
			for (int j = 0; j < boardSize/2; j++) {
				aux = board[i][j];
				board[i][j] = board[i][boardSize-1-j];
				board[i][boardSize-1-j] = aux;
			}
		}
	}
	
	private void trans() {
		Cell aux;
		for (int i = 0; i < boardSize; i++) {
			for (int j = i; j < boardSize; j++) {
				aux = board[i][j];
				board[i][j] = board[j][i];
				board[j][i] = aux;
			}
		}
	}

	public Position freePosition(Random rand){
		int freeCounter = 0;
		Position free[] = new Position[boardSize * boardSize];
		for (int i = 0; i < boardSize; i++) {
			for (int j = 0; j < boardSize; j++) {
				if (board[i][j].isEmpty()){
					free[freeCounter] = new Position(i,j);
					freeCounter++;
				}
			}
		}

		return free[rand.nextInt(freeCounter)];
	}
	
	public boolean canMerge(GameRules rules) {
		boolean canMerge = false;
		int i = 0, j = 0;

		while (!canMerge && i < boardSize - 1) {
			while (!canMerge && j < boardSize - 1) {
				if (board[i][j].canMerge(board[i][j + 1], rules)) {
					canMerge = true;
				} else if (board[i][j].canMerge(board[i + 1][j], rules)) {
					canMerge = true;
				}
				j++;
			}
			i++;
			j = 0;
		}

		while (!canMerge && j < boardSize - 1) {
			if (board[j][boardSize - 1].canMerge(board[j + 1][boardSize - 1], rules)) {
				canMerge = true;
			} else if (board[boardSize - 1][j].canMerge(board[boardSize - 1][j + 1], rules)) {
				canMerge = true;
			}
			j++;
		}
		
		return canMerge;
	}

	public boolean isFull() {
		boolean isFull = true;
		int i = 0, j = 0;

		while (isFull && i < boardSize) {
			while (isFull && j < boardSize) {
				if (board[i][j].isEmpty()) {
					isFull = false;
				}
				j++;
			}
			i++;
			j = 0;
		}

		return isFull;
	}

	@Override
	public String toString() {
		String result = "";
		int cellSize = 7;
		String space = " ";
		String vDelimiter = "|";
		String hDelimiter = "-";
	

		for (int i = 0; i < boardSize; i++) {
			result += "\n" + MyStringUtils.repeat(hDelimiter, boardSize * cellSize + boardSize + 1) + "\n";
			for (int j = 0; j < boardSize; j++) {
				result += vDelimiter;
				result += MyStringUtils.centre(board[i][j].toString(), cellSize);
			}
			result += vDelimiter;
		}
		result += "\n" + MyStringUtils.repeat(hDelimiter, boardSize * cellSize + boardSize + 1) + "\n";
		return result;
	}	

	public int getMax() {
		int max = 0;

		for (int i = 0; i < boardSize; i++) {
			for (int j = 0; j < boardSize; j++) {
				if (max < board[i][j].getValue()) {
					max = board[i][j].getValue();
				}
			}
		}

		return max;
	}

	public int getMin() {
		int min = 2048;

		for (int i = 0; i < boardSize; i++) {
			for (int j = 0; j < boardSize; j++) {
				if (board[i][j].getValue() != 0 && min > board[i][j].getValue()) {
					min = board[i][j].getValue();
				}
			}
		}

		return min;
	}

	public int[][] getState(){
		int[][] boardState = new int[boardSize][boardSize];
		for (int i = 0; i < boardSize; i++) {
			for (int j = 0; j < boardSize; j++) {
				boardState[i][j] = board[i][j].getValue();
			}
		}

		return boardState;
	}

	public void setState(int[][] aState){
		for (int i = 0; i < boardSize; i++) {
			for (int j = 0; j < boardSize; j++) {
				board[i][j].setValue(aState[i][j]);
			}
		}
	}

}
