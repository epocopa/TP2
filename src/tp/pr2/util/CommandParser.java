/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr2.util;

import tp.pr2.control.Controller;
import tp.pr2.control.commands.*;

public class CommandParser {
	private static Command[] availableCommands = { new HelpCommand(), new ResetCommand(), new ExitCommand(),
												   new MoveCommmand(), new UndoCommand(), new RedoCommand(),
												   new PlayCommand() };

	public static Command parseCommand(String[] commandWords, Controller controller){
		Command command = null;
		for(Command c : availableCommands) {
			command = c.parse(commandWords, controller);
			if (command != null){
				return command;
			}
		}
		return command;
	}
	public static String commandHelp(){
		String help = "";
		for(Command c : availableCommands){
			help += c.helpText() + "\n";
		}
		return help;
	}
}
