/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr2;

import tp.pr2.control.Controller;
import tp.pr2.multigames.Game;
import tp.pr2.multigames.Rules2048;

import java.util.Scanner;
import java.util.Random;


public class Game2048 {

	public static void main(String[] args){
		int size = 0, numInitial = 0;
		long seed = 0;
		Game game = null;
		Scanner in = new Scanner(System.in);


		if (args.length >= 2){
			size = Integer.parseInt(args[0]);
			numInitial = Integer.parseInt(args[1]);
			 if(args.length == 3){
				seed = Integer.parseInt(args[2]);
			 } else	{
			 	seed = new Random().nextInt(1000);
			 }
			game = new Game(size, numInitial, seed, new Rules2048());
			System.out.println(size + " " + numInitial);
			Controller controller = new Controller(game, in);
			controller.run();
		}
	}

}
