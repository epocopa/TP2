/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr2.multigames;

import tp.pr2.*;

import java.util.Random;

public class Game {
	private Board board;
	private int size, initCells, score, bestValue, freeCounter;
	private Random random;
	private GameStateStack undoStack = new GameStateStack();
	private GameStateStack redoStack = new GameStateStack();
	private GameRules currentRules;

	public Game(int size, int initCells, long seed, GameRules rules) {
		this.size = size;
		this.initCells = initCells;
		this.random = new Random(seed);
		currentRules = rules;
		board = currentRules.createBoard(size);
		currentRules.initBoard(board, initCells, random);
		bestValue = currentRules.getWinValue(board);
	}

	public void move(Direction dir){
		undoStack.push(getState());
		if (!redoStack.isEmpty()){
			redoStack = new GameStateStack();
		}

		MoveResults m = board.executeMove(dir, currentRules);
		if (m.isMoved()){
			score += m.getPoints();
			currentRules.addNewCell(board, random);
			bestValue = currentRules.getWinValue(board);
		}
	}

	public void reset(){
		board = currentRules.createBoard(size);
		currentRules.initBoard(board, initCells, random);
		score = 0;
		bestValue = currentRules.getWinValue(board);
		undoStack = new GameStateStack();
		redoStack = new GameStateStack();
	}

	/**
	 * push(estado actual) en la pila de redo
	 * estado actual = pop() de la pila de undo
	 * @return undone successfully
	 */
	public boolean undo(){
		if (!undoStack.isEmpty()){
			redoStack.push(getState());
			setState(undoStack.pop());
			return true;
		} else{
			System.out.println("Undo is not available");
			return false;
		}
	}

	/**
	 * push(estado actual) en la pila de undo
	 * estado actual = pop() de la pila de redo
	 * @return redone successfully
	 */
	public boolean redo(){
		if (!redoStack.isEmpty()){
			undoStack.push(getState());
			setState(redoStack.pop());
			return true;
		} else {
			System.out.println("Nothing to redo");
			return false;
		}
	}

	public GameState getState(){
		GameState gs = new GameState();
		gs.setBoardState(board.getState());
		gs.setHighest(bestValue);
		gs.setScore(score);
		return  gs;
	}

	public void setState(GameState aState){
		board.setState(aState.getBoardState());
		bestValue= aState.getHighest();
		score = aState.getScore();
	}

	@Override
	public String toString() {
		return board.toString() +"\nScore: " + score + " Best value: " + bestValue;
	}

	public boolean isEnded(){
		return currentRules.lose(board) || currentRules.win(board);
	}

	public boolean isWin() {
		return currentRules.win(board);
	}
}
