/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr2.multigames;

import tp.pr2.Board;
import tp.pr2.Cell;
import tp.pr2.Position;
import java.util.Random;

import static tp.pr2.util.MyMathUtils.nextFib;

public class RulesFib implements GameRules {
	private final int winValue = 144;

	public void addNewCellAt(Board board, Position pos, Random rand) {
		float rdn = rand.nextFloat();
		int value = 1;

		if (rdn > 0.9) {
			value = 2;
		}

		board.setCell(pos, value);
	}

	public int getWinValue(Board board) {
		return board.getMax();
	}

	public boolean win(Board board) {
		return getWinValue(board) == winValue;
	}

	public boolean canMerge(Cell self, Cell other) {
		boolean canMerge = false;

		if (self.getValue() < other.getValue()) {
			canMerge = nextFib(self.getValue()) == other.getValue();
		} else if (self.getValue() == 1) {
			canMerge = true;
		} else {
			canMerge = nextFib(other.getValue()) == self.getValue();
		}

		return canMerge;
	}
}
