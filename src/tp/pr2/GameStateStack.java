/*
    This file is part of 2048.

    2048 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    2048 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with 2048.  If not, see <http://www.gnu.org/licenses/>.
*/

package tp.pr2;

public class GameStateStack {
	private static final int CAPACITY = 20;
	private GameState[] buffer = new GameState[CAPACITY];
	private int count;
	/**
	 * @return Devuelve el último estado almacenado
	 */
	public GameState pop(){
		count--;
		return buffer[count];
	}

	/**
	 * @return Almacena un nuevo estado
	 */
	public void push(GameState state){
		if (count < CAPACITY){
			buffer[count] = state;
			count++;
		} else {
			for (int i = CAPACITY-1; i > 0 ; i--) {
				buffer[i-1]=buffer[i];
			}
			buffer[count-1] = state;
		}
	}

	/**
	 * @return Devuelve true si la secuencia está vacía
	 */
	public boolean isEmpty() {
		return count == 0;
	}
}
